Given(/^I see Login Page is opened$/) do
  @login_page = LoginPage.new
  @login_page.load

  expect(@login_page).to have_content 'Redmine'
  sleep 2 # This is required just to see visually that action was actually performed
end

When(/^I fill in login form and click log in button$/) do
  login_user 'user'
end

Then(/^I became logged in user$/) do
  expect(@login_page).to have_content 'Logged in as user'
  sleep 2 # This is required just to see visually that login was actually performed
end