module FeatureHelper
  def login_user(username)
    @login_page.sign_in_button.click
    expect(current_url).to include '/login'

    @login_page.email_field.set username
    @login_page.password_field.set '8ktcbP4cxpWV'
    @login_page.log_in_btn.click
  end
end
