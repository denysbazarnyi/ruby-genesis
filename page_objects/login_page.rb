class LoginPage < SitePrism::Page
  set_url 'http://192.168.56.102'

  element :sign_in_button, :xpath, '//*[@id="account"]/ul/li[1]/a'

  element :email_field, '#username'
  element :password_field, '#password'
  element :log_in_btn, '#login-submit'
end