feature 'the signin process', js: true do
  scenario 'signs me in' do
    @login_page = LoginPage.new
    @login_page.load

    # visit 'http://192.168.56.102/'
    expect(page).to have_content 'Redmine'

    sleep 2 # This is required just to see visually that action was actually performed

=begin
    click_link 'Sign in'
    @login_page.sign_in_button.click

    expect(current_url).to include '/login'

    # find('#username').set 'user'
    # find('#password').set '8ktcbP4cxpWV'
    # click_button 'login' #
    @login_page.email_field.set 'user'
    @login_page.password_field.set '8ktcbP4cxpWV'
    @login_page.log_in_btn.click
=end

    login_user 'user'

    expect(page).to have_content 'Logged in as user'

    sleep 2 # This is required just to see visually that login was actually performed
  end
end